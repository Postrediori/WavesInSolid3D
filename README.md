# Waves on Surfaces

Simulation of elastic waves in elastic 3D solids written in JavaScript and WebGL.

# Wave types

## Love Waves

![Love Wave Screenshot](images/LoveWaveScreenshot.png)

Depiction of the propagation of a [Love wave](https://en.wikipedia.org/wiki/Love_wave), named after
[Augustus Edward Hough Love](https://en.wikipedia.org/wiki/Augustus_Edward_Hough_Love).
The particles vibrate perpendicularly to the direction of propagation
and the amplitude decays with depth.

## Rayleigh Waves

![Rayleigh Wave Screenshot](images/RayleighWaveScreenshot.png)

[Rayleigh waves](https://en.wikipedia.org/wiki/Rayleigh_wave)
(names after [Lord Rayleigh](https://en.wikipedia.org/wiki/Lord_Rayleigh)) are a type of surface
acoustic wave that travel along the surface of solids.
There are two components in a motion of a particle disturbed by a Rayleigh wave:
horizontal and vertical that totals in movement along an ellipse.
