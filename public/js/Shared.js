'use strict'

/*
 * Constants
 */

const INITIAL_AMPLITUDE = 1.5,
    INITIAL_VELOCITY = 2.5,
    INITIAL_PERIOD = 2.5,
    INITIAL_DISSIPATION = 1.5,
    MAX_VELOCITY = 10.0,
    MAX_DISSIPATION = 5.0,
    MIN_PERIOD = 1.5,
    MAX_PERIOD = 5.0;

const OVERLAY_DIV_ID = 'overlay',
    UI_DIV_ID = 'ui',
    CAMERA_DIV_ID = 'camera',
    SIMULATOR_CANVAS_ID = 'simulator';

const COORD_SIZE = 4;
const X_INDEX = 0,
    Y_INDEX = 1,
    Z_INDEX = 2,
    W_INDEX = 3;

const R_INDEX = 0,
    THETA_INDEX = 1;

const INDICES_PER_LINE = 2,
    INDICES_PER_TILE = 6;

const CLEAR_COLOR = [0.75, 0.75, 0.75, 1.0];

const QUAD_COLOR = new Float32Array([0.9, 0.9, 0.9]);
const OUTLINE_COLOR = new Float32Array([0.1, 0.1, 0.1]);

const A_VERTEX_POSITION = 0;

const SIZE_OF_FLOAT = 4;

const FOV = (60 / 180) * Math.PI,
    NEAR = 1,
    FAR = 10000,
    MIN_ASPECT = 16 / 9;

const CAMERA_SENSITIVITY = 4.0;

const CAMERA_MODE_NONE = 0,
    CAMERA_MODE_ORBITING = 1;

const CAMERA_DISTANCE = 50.0,
    ORBIT_POINT = [0.0, 0.0, 0.0],
    INITIAL_AZIMUTH = 0.4,
    INITIAL_ELEVATION = 0.5,
    MIN_AZIMUTH = 0.25,
    MAX_AZIMUTH = 1.5,
    MIN_ELEVATION = 0.0,
    MAX_ELEVATION = 1.25;
    
const DEFAULT_UI_COLOR = 'rgb(48, 113, 191)',
    BUTTON_ACTIVE_COLOR = 'white',
    BUTTON_COLOR = '#ffffff',
    BUTTON_BACKGROUND = '#555555';

/*
 * GL Routines
 */

function buildShader(gl, type, source) {
    let shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    return shader;
}

function buildProgramWrapper(gl, vertexShader, fragmentShader, attributeLocations) {
    let programWrapper = {};

    let program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    for (let attributeName in attributeLocations) {
        gl.bindAttribLocation(program, attributeLocations[attributeName], attributeName);
    }
    gl.linkProgram(program);
    let uniformLocations = {};
    let numberOfUniforms = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);
    for (let i = 0; i < numberOfUniforms; i += 1) {
        let activeUniform = gl.getActiveUniform(program, i),
            uniformLocation = gl.getUniformLocation(program, activeUniform.name);
        uniformLocations[activeUniform.name] = uniformLocation;
    }

    programWrapper.program = program;
    programWrapper.uniformLocations = uniformLocations;

    return programWrapper;
}

/*
 * Math Routines
 */

function clamp(x, min, max) {
    return Math.min(Math.max(x, min), max);
};

/*
 * Matrix Routines
 */

function coordAdd(out, coordA, coordB) {
    out[X_INDEX] = coordA[X_INDEX] + coordB[X_INDEX];
    out[Y_INDEX] = coordA[Y_INDEX] + coordB[Y_INDEX];
    out[Z_INDEX] = coordA[Z_INDEX] + coordB[Z_INDEX];
    out[W_INDEX] = coordA[W_INDEX] + coordB[W_INDEX];
    return out;
}

function coordSub(out, coordA, coordB) {
    out[X_INDEX] = coordA[X_INDEX] - coordB[X_INDEX];
    out[Y_INDEX] = coordA[Y_INDEX] - coordB[Y_INDEX];
    out[Z_INDEX] = coordA[Z_INDEX] - coordB[Z_INDEX];
    out[W_INDEX] = coordA[W_INDEX] - coordB[W_INDEX];
    return out;
}

function convertCartesianToRadial(radialCoord, cartesianCoord, origin) {
    coordSub(radialCoord, cartesianCoord, origin);

    let dx = radialCoord[X_INDEX],
        dy = radialCoord[Z_INDEX];
    let r = Math.sqrt(dx * dx + dy * dy);
    let theta = Math.atan2(radialCoord[Z_INDEX], radialCoord[X_INDEX]);

    radialCoord[R_INDEX] = r;
    radialCoord[THETA_INDEX] = theta;

    return radialCoord;
}

function makeIdentityMatrix(matrix) {
    matrix[0] = 1.0;
    matrix[1] = 0.0;
    matrix[2] = 0.0;
    matrix[3] = 0.0;
    matrix[4] = 0.0;
    matrix[5] = 1.0;
    matrix[6] = 0.0;
    matrix[7] = 0.0;
    matrix[8] = 0.0;
    matrix[9] = 0.0;
    matrix[10] = 1.0;
    matrix[11] = 0.0;
    matrix[12] = 0.0;
    matrix[13] = 0.0;
    matrix[14] = 0.0;
    matrix[15] = 1.0;
    return matrix;
}

function makeXRotationMatrix(matrix, angle) {
    matrix[0] = 1.0;
    matrix[1] = 0.0;
    matrix[2] = 0.0;
    matrix[3] = 0.0;
    matrix[4] = 0.0;
    matrix[5] = Math.cos(angle);
    matrix[6] = Math.sin(angle);
    matrix[7] = 0.0;
    matrix[8] = 0.0;
    matrix[9] = -Math.sin(angle);
    matrix[10] = Math.cos(angle);
    matrix[11] = 0.0;
    matrix[12] = 0.0;
    matrix[13] = 0.0;
    matrix[14] = 0.0;
    matrix[15] = 1.0;
    return matrix;
}

function makeYRotationMatrix(matrix, angle) {
    matrix[0] = Math.cos(angle);
    matrix[1] = 0.0;
    matrix[2] = -Math.sin(angle);
    matrix[3] = 0.0;
    matrix[4] = 0.0;
    matrix[5] = 1.0;
    matrix[6] = 0.0;
    matrix[7] = 0.0;
    matrix[8] = Math.sin(angle);
    matrix[9] = 0.0;
    matrix[10] = Math.cos(angle);
    matrix[11] = 0.0;
    matrix[12] = 0.0;
    matrix[13] = 0.0;
    matrix[14] = 0.0;
    matrix[15] = 1.0;
    return matrix;
}

function premultiplyMatrix(out, matrixA, matrixB) {
    let b0 = matrixB[0], b4 = matrixB[4], b8 = matrixB[8], b12 = matrixB[12],
        b1 = matrixB[1], b5 = matrixB[5], b9 = matrixB[9], b13 = matrixB[13],
        b2 = matrixB[2], b6 = matrixB[6], b10 = matrixB[10], b14 = matrixB[14],
        b3 = matrixB[3], b7 = matrixB[7], b11 = matrixB[11], b15 = matrixB[15],
        aX = matrixA[0], aY = matrixA[1], aZ = matrixA[2], aW = matrixA[3];

    out[0] = b0 * aX + b4 * aY + b8 * aZ + b12 * aW;
    out[1] = b1 * aX + b5 * aY + b9 * aZ + b13 * aW;
    out[2] = b2 * aX + b6 * aY + b10 * aZ + b14 * aW;
    out[3] = b3 * aX + b7 * aY + b11 * aZ + b15 * aW;

    aX = matrixA[4], aY = matrixA[5], aZ = matrixA[6], aW = matrixA[7];
    out[4] = b0 * aX + b4 * aY + b8 * aZ + b12 * aW;
    out[5] = b1 * aX + b5 * aY + b9 * aZ + b13 * aW;
    out[6] = b2 * aX + b6 * aY + b10 * aZ + b14 * aW;
    out[7] = b3 * aX + b7 * aY + b11 * aZ + b15 * aW;

    aX = matrixA[8], aY = matrixA[9], aZ = matrixA[10], aW = matrixA[11];
    out[8] = b0 * aX + b4 * aY + b8 * aZ + b12 * aW;
    out[9] = b1 * aX + b5 * aY + b9 * aZ + b13 * aW;
    out[10] = b2 * aX + b6 * aY + b10 * aZ + b14 * aW;
    out[11] = b3 * aX + b7 * aY + b11 * aZ + b15 * aW;

    aX = matrixA[12], aY = matrixA[13], aZ = matrixA[14], aW = matrixA[15];
    out[12] = b0 * aX + b4 * aY + b8 * aZ + b12 * aW;
    out[13] = b1 * aX + b5 * aY + b9 * aZ + b13 * aW;
    out[14] = b2 * aX + b6 * aY + b10 * aZ + b14 * aW;
    out[15] = b3 * aX + b7 * aY + b11 * aZ + b15 * aW;

    return out;
}

function makePerspectiveMatrix(matrix, fov, aspect, near, far) {
    let f = Math.tan(0.5 * (Math.PI - fov)),
        range = near - far;

    matrix[0] = f / aspect;
    matrix[1] = 0;
    matrix[2] = 0;
    matrix[3] = 0;
    matrix[4] = 0;
    matrix[5] = f;
    matrix[6] = 0;
    matrix[7] = 0;
    matrix[8] = 0;
    matrix[9] = 0;
    matrix[10] = far / range;
    matrix[11] = -1;
    matrix[12] = 0;
    matrix[13] = 0;
    matrix[14] = (near * far) / range;
    matrix[15] = 0.0;

    return matrix;
}

/*
 * UI Routines
 */

function setPerspective(element, value) {
    element.style.WebkitPerspective = value;
    element.style.perspective = value;
}

function getMousePosition(event, element) {
    var boundingRect = element.getBoundingClientRect();
    return {
        x: event.clientX - boundingRect.left,
        y: event.clientY - boundingRect.top
    };
}
