'use strict'

function main () {
    let simulatorCanvas = document.getElementById(SIMULATOR_CANVAS_ID),
        overlayDiv = document.getElementById(OVERLAY_DIV_ID),
        uiDiv = document.getElementById(UI_DIV_ID);

    let gl = simulatorCanvas.getContext('webgl') || simulatorCanvas.getContext('experimental-webgl');
    if (!gl) {
        console.log("No WebGL");
        return;
    }
    else {
        console.log("WebGL Ver: "+gl.getParameter(gl.VERSION));
        console.log("WebGL Shader Language Ver: "+gl.getParameter(gl.SHADING_LANGUAGE_VERSION));
    }
    let simulator = new Simulator(gl, simulatorCanvas, 640, 480);

    let velocitySlider = new Slider(document.getElementById('velocity-slider'), 0.0, MAX_VELOCITY, INITIAL_VELOCITY, function (value) {
        simulator.setVelocity(value);
    });

    let periodSlider = new Slider(document.getElementById('period-slider'), MIN_PERIOD, MAX_PERIOD, INITIAL_PERIOD, function (value) {
        simulator.setPeriod(value);
    });

    let dissipationSlider = new Slider(document.getElementById('dissipation-slider'), 0.0, MAX_DISSIPATION, INITIAL_DISSIPATION, function (value) {
        simulator.setDissipation(value);
    });

    let waveButtons = new Buttons([
        document.getElementById('wave-type-love'),
        document.getElementById('wave-type-rayleigh'),
        document.getElementById('wave-type-pwave'),
        document.getElementById('wave-type-swave'),
        document.getElementById('wave-type-asym-lamb'),
        document.getElementById('wave-type-sym-lamb'),
        document.getElementById('wave-type-radial-pwave'),
        document.getElementById('wave-type-radial-swave')
    ], function (index) {
        simulator.setModel(index);
    });

    let geometryButtons = new Buttons([
        document.getElementById('geometry-type-beam'),
        document.getElementById('geometry-type-plate')
    ], function (index) {
        simulator.setGeometry(index);
    });

    velocitySlider.setColor(DEFAULT_UI_COLOR);
    periodSlider.setColor(DEFAULT_UI_COLOR);
    dissipationSlider.setColor(DEFAULT_UI_COLOR);
    waveButtons.setColor(DEFAULT_UI_COLOR);
    geometryButtons.setColor(DEFAULT_UI_COLOR);

    let camera = new Camera();
    let projectionMatrix = makePerspectiveMatrix(new Float32Array(16), FOV, MIN_ASPECT, NEAR, FAR);
    
    let width = window.innerWidth,
        height = window.innerHeight;

    // Mouse controls
    let lastMouseX = 0;
    let lastMouseY = 0;
    let mode = CAMERA_MODE_NONE;

    let onMouseDown = function (event) {
        event.preventDefault();

        var mousePosition = getMousePosition(event, uiDiv);
        var mouseX = mousePosition.x,
            mouseY = mousePosition.y;

        mode = CAMERA_MODE_ORBITING;
        lastMouseX = mouseX;
        lastMouseY = mouseY;
    };
    overlayDiv.addEventListener('mousedown', onMouseDown, false);

    overlayDiv.addEventListener('mousemove', function (event) {
        event.preventDefault();

        let mousePosition = getMousePosition(event, uiDiv),
            mouseX = mousePosition.x,
            mouseY = mousePosition.y;

        if (mode === CAMERA_MODE_ORBITING) {
            overlayDiv.style.cursor = '-webkit-grabbing';
            overlayDiv.style.cursor = '-moz-grabbing';
            overlayDiv.style.cursor = 'grabbing';
        }
        else {
            overlayDiv.style.cursor = '-webkit-grab';
            overlayDiv.style.cursor = '-moz-grab';
            overlayDiv.style.cursor = 'grab';
        }

        if (mode === CAMERA_MODE_ORBITING) {
            camera.changeAzimuth((mouseX - lastMouseX) / width * CAMERA_SENSITIVITY);
            camera.changeElevation((mouseY - lastMouseY) / height * CAMERA_SENSITIVITY);
            lastMouseX = mouseX;
            lastMouseY = mouseY;
        }
    });

    overlayDiv.addEventListener('mouseup', function (event) {
        event.preventDefault();
        mode = CAMERA_MODE_NONE;
    });

    window.addEventListener('mouseout', function (event) {
        let from = event.relatedTarget || event.toElement;
        if (!from || from.nodeName === 'HTML') {
            mode = CAMERA_MODE_NONE;
        }
    });

    // Touch controls
    overlayDiv.addEventListener('touchstart', function (event) {
        event.preventDefault();

        const touch = event.changedTouches[0];

        mode = CAMERA_MODE_ORBITING;
        lastMouseX = touch.pageX;
        lastMouseY = touch.pageY;
    });

    overlayDiv.addEventListener('touchend', function (event) {
        event.preventDefault();
        mode = CAMERA_MODE_NONE;
    });

    overlayDiv.addEventListener('touchcancel', function (event) {
        event.preventDefault();
        mode = CAMERA_MODE_NONE;
    });

    overlayDiv.addEventListener('touchmove', function (event) {
        event.preventDefault();

        const touch = event.changedTouches[0];
        const mouseX = touch.pageX,
            mouseY = touch.pageY;

        if (mode === CAMERA_MODE_ORBITING) {
            camera.changeAzimuth((mouseX - lastMouseX) / width * CAMERA_SENSITIVITY);
            camera.changeElevation((mouseY - lastMouseY) / height * CAMERA_SENSITIVITY);
            lastMouseX = mouseX;
            lastMouseY = mouseY;
        }
    });

    // Resize
    let setUIPerspective = function (height) {
        let fovValue = 0.5 / Math.tan(FOV / 2) * height;
        setPerspective(uiDiv, fovValue + 'px');
    };

    let onresize = function () {
        let windowWidth = window.innerWidth,
            windowHeight = window.innerHeight;

        overlayDiv.style.width = windowWidth + 'px';
        overlayDiv.style.height = windowHeight + 'px';

        if (windowWidth / windowHeight > MIN_ASPECT) {
            makePerspectiveMatrix(projectionMatrix, FOV, windowWidth / windowHeight, NEAR, FAR);
            simulator.resize(windowWidth, windowHeight);
            uiDiv.style.width = windowWidth + 'px';
            uiDiv.style.height = windowHeight + 'px';
            simulatorCanvas.style.top = '0px';
            uiDiv.style.top = '0px';
            setUIPerspective(windowHeight);
            width = windowWidth;
            height = windowHeight;
        } else {
            var newHeight = windowWidth / MIN_ASPECT;
            makePerspectiveMatrix(projectionMatrix, FOV, windowWidth / newHeight, NEAR, FAR);
            simulator.resize(windowWidth, newHeight);
            simulatorCanvas.style.top = (windowHeight - newHeight) * 0.5 + 'px';
            uiDiv.style.top = (windowHeight - newHeight) * 0.5 + 'px';
            setUIPerspective(newHeight);
            uiDiv.style.width = windowWidth + 'px';
            uiDiv.style.height = newHeight + 'px';
            width = windowWidth;
            height = newHeight;
        }
    };

    window.addEventListener('resize', onresize);
    onresize();

    // Main loop
    let lastTime = (new Date()).getTime();
    let render = function render (currentTime) {
        let deltaTime = (currentTime - lastTime) / 1000 || 0.0;
        lastTime = currentTime;

        simulator.update(deltaTime);
        simulator.render(projectionMatrix, camera.getViewMatrix());

        requestAnimationFrame(render);
    };
    render();
}

main();
