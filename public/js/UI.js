
class Slider {

constructor(element, min, max, initialValue, changeCallback) {
    this.div = element;

    this.innerDiv = document.createElement('div');
    this.innerDiv.style.position = 'absolute';
    this.innerDiv.style.height = this.div.offsetHeight + 'px';

    this.div.appendChild(this.innerDiv);

    this.color = 'black';

    this.min = min;
    this.max = max;
    this.value = initialValue;

    this.changeCallback = changeCallback;

    this.mousePressed = false;

    this.redraw();

    let obj = this; // Object reference for callbacks
    this.div.addEventListener('mousedown', function (event) {
        obj.mousePressed = true;
        obj.onChange(event);
    });

    document.addEventListener('mouseup', function (event) {
        obj.mousePressed = false;
    });

    document.addEventListener('mousemove', function (event) {
        if (obj.mousePressed) {
            obj.onChange(event);
        }
    });
}

    setColor(newColor) {
        this.color = newColor;
        this.redraw();
    };

    getValue() {
        return this.value;
    };

    redraw() {
        let fraction = (this.value - this.min) / (this.max - this.min);
        this.innerDiv.style.background = this.color;
        this.innerDiv.style.width = fraction * this.div.offsetWidth + 'px';
        this.innerDiv.style.height = this.div.offsetHeight + 'px';
    };

    onChange(event) {
        let mouseX = getMousePosition(event, this.div).x;

        this.value = clamp((mouseX / this.div.offsetWidth) * (this.max - this.min) + this.min, this.min, this.max);

        this.changeCallback(this.value);

        this.redraw();
    };
};

class Buttons {

constructor(elements, changeCallback) {
    this.elements = elements;
    this.activeElement = elements[0];

    this.color = 'black';

    let obj = this;
    for (let i = 0; i < elements.length; ++i) {
        (function () { //create closure to store index
            let index = i;
            let clickedElement = obj.elements[i];
            obj.elements[i].addEventListener('click', function () {
                if (obj.activeElement !== clickedElement) {
                    obj.activeElement = clickedElement;

                    changeCallback(index);

                    obj.refresh();
                }

            });
        }());
    }
}

    setColor(newColor) {
        this.color = newColor;
        this.refresh();
    };

    refresh() {
        for (let i = 0; i < this.elements.length; ++i) {
            if (this.elements[i] === this.activeElement) {
                this.elements[i].style.color = BUTTON_ACTIVE_COLOR;
                this.elements[i].style.background = this.color;
            } else {
                this.elements[i].style.color = BUTTON_COLOR;
                this.elements[i].style.background = BUTTON_BACKGROUND;
            }
        }
    };
};
