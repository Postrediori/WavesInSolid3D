'use strict'

const QUAD_VERTEX_SOURCE = `#version 100
precision highp float;

attribute vec3 a_position;
attribute vec3 a_texCoord;

uniform mat4 u_projection;
uniform mat4 u_view;

void main (void) {
    vec3 position = a_position;
    gl_Position = u_projection * u_view * vec4(a_position, 1.0);
}`;

const QUAD_FRAGMENT_SOURCE = `#version 100
precision highp float;

uniform vec3 u_color;

void main (void) {
    gl_FragColor = vec4(u_color, 1.0);
}`;

class Simulator {

    static gl;

constructor(gl, canvas, width, height) {
    this.canvas = canvas;
    this.canvas.width = width;
    this.canvas.height = height;

    Simulator.gl = gl;

    this.waveModels = [
        new LoveWaveModel(),
        new RayleighWaveModel(),
        new PressureWaveModel(),
        new ShearWaveModel(),
        new AsymLambWaveModel(),
        new SymLambWaveModel(),
        new RadialPressureWaveModel(),
        new RadialShearWaveModel()
    ];

    this.waveModel = this.waveModels[0];

    this.currentTimeFactor = 0.0;
    this.waveVelocity = INITIAL_VELOCITY;

    gl.clearColor.apply(gl, CLEAR_COLOR);
    gl.enable(gl.DEPTH_TEST);

    this.quadProgram = gl.createProgram();
    this.quadProgram = buildProgramWrapper(gl,
        buildShader(gl, gl.VERTEX_SHADER, QUAD_VERTEX_SOURCE),
        buildShader(gl, gl.FRAGMENT_SHADER, QUAD_FRAGMENT_SOURCE),
        {"a_position" : A_VERTEX_POSITION});

    gl.enableVertexAttribArray(A_VERTEX_POSITION);

    // Create geometries
    const GEOMETRY_RESOLUTION = 10,
        GEOMETRY_SCALE = 1.0;

    this.geometries = [
        new Geometry(gl, "Beam", this.quadProgram,
            [GEOMETRY_RESOLUTION, GEOMETRY_RESOLUTION, GEOMETRY_RESOLUTION * 5],
            [GEOMETRY_RESOLUTION * GEOMETRY_SCALE, GEOMETRY_RESOLUTION * GEOMETRY_SCALE, GEOMETRY_RESOLUTION * 5 * GEOMETRY_SCALE]),
        new Geometry(gl, "Plate", this.quadProgram,
            [GEOMETRY_RESOLUTION * 4, Math.floor(GEOMETRY_RESOLUTION/2), GEOMETRY_RESOLUTION * 4],
            [GEOMETRY_RESOLUTION * 4 * GEOMETRY_SCALE, Math.floor(GEOMETRY_RESOLUTION/2) * GEOMETRY_SCALE, GEOMETRY_RESOLUTION * 4 * GEOMETRY_SCALE])
    ];

    this.geometry = this.geometries[0];
}

    setVelocity(newVelocity) {
        this.waveVelocity = newVelocity;
    };

    setPeriod(newPeriod) {
        for (let i = 0; i < this.waveModels.length; i++) {
            this.waveModels[i].setPeriod(newPeriod);
        }
    };

    setDissipation(newDissipation) {
        for (let i = 0; i < this.waveModels.length; i++) {
            this.waveModels[i].setDissipation(newDissipation);
        }
    };
    
    setModel(newModel) {
        this.waveModel = this.waveModels[newModel];
    };
    
    setGeometry(newGeometry) {
        this.geometry = this.geometries[newGeometry];
    }

    resize(width, height) {
        this.canvas.width = width;
        this.canvas.height = height;
    };

    update(deltaTime) {
        this.currentTimeFactor += this.waveVelocity * deltaTime;

        this.geometry.update(this.waveModel, this.currentTimeFactor);
    }

    render(projectionMatrix, viewMatrix) {
        let gl = Simulator.gl;

        gl.viewport(0, 0, this.canvas.width, this.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        this.geometry.render(projectionMatrix, viewMatrix);
    }
}
