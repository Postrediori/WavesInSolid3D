'use strict'

class Camera {
    constructor() {
        this.azimuth = INITIAL_AZIMUTH;
        this.elevation = INITIAL_ELEVATION;

        this.viewMatrix = makeIdentityMatrix(new Float32Array(16));
        this.position = new Float32Array(3);
        this.changed = true;

        this.orbitTranslationMatrix = makeIdentityMatrix(new Float32Array(16));
        this.xRotationMatrix = new Float32Array(16);
        this.yRotationMatrix = new Float32Array(16);
        this.distanceTranslationMatrix = makeIdentityMatrix(new Float32Array(16));
    }

    changeAzimuth(deltaAzimuth) {
        this.azimuth += deltaAzimuth;
        this.azimuth = clamp(this.azimuth, MIN_AZIMUTH, MAX_AZIMUTH);
        this.changed = true;
    };

    changeElevation(deltaElevation) {
        this.elevation += deltaElevation;
        this.elevation = clamp(this.elevation, MIN_ELEVATION, MAX_ELEVATION);
        this.changed = true;
    };

    getPosition() {
        return this.position;
    };

    getViewMatrix() {
        if (this.changed) {
            makeIdentityMatrix(this.viewMatrix);

            makeXRotationMatrix(this.xRotationMatrix, this.elevation);
            makeYRotationMatrix(this.yRotationMatrix, this.azimuth);
            this.distanceTranslationMatrix[14] = -CAMERA_DISTANCE;
            this.orbitTranslationMatrix[12] = -ORBIT_POINT[0];
            this.orbitTranslationMatrix[13] = -ORBIT_POINT[1];
            this.orbitTranslationMatrix[14] = -ORBIT_POINT[2];

            premultiplyMatrix(this.viewMatrix, this.viewMatrix, this.orbitTranslationMatrix);
            premultiplyMatrix(this.viewMatrix, this.viewMatrix, this.yRotationMatrix);
            premultiplyMatrix(this.viewMatrix, this.viewMatrix, this.xRotationMatrix);
            premultiplyMatrix(this.viewMatrix, this.viewMatrix, this.distanceTranslationMatrix);

            this.position[0] = CAMERA_DISTANCE * Math.sin(Math.PI / 2 - this.elevation) * Math.sin(-this.azimuth) + ORBIT_POINT[0];
            this.position[1] = CAMERA_DISTANCE * Math.cos(Math.PI / 2 - this.elevation) + ORBIT_POINT[1];
            this.position[2] = CAMERA_DISTANCE * Math.sin(Math.PI / 2 - this.elevation) * Math.cos(-this.azimuth) + ORBIT_POINT[2];

            this.changed = false;
        }

        return this.viewMatrix;
    };
};
