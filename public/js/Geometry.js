'use strict'

// Calculate indices for quads
function createQuadIndices(nDiv, mDiv) {
    let indices = new Uint16Array((nDiv - 1) * (mDiv - 1) * INDICES_PER_TILE);
    for (let mIndex = 0; mIndex < mDiv - 1; mIndex += 1) {
        for (let nIndex = 0; nIndex < nDiv - 1; nIndex += 1) {
            let idx = (mIndex * (nDiv - 1) + nIndex) * INDICES_PER_TILE;
            let topLeft = mIndex * nDiv + nIndex,
                topRight = topLeft + 1,
                bottomLeft = topLeft + nDiv,
                bottomRight = bottomLeft + 1;

            indices[idx  ] = topLeft;
            indices[idx+1] = bottomLeft;
            indices[idx+2] = bottomRight;
            indices[idx+3] = bottomRight;
            indices[idx+4] = topRight;
            indices[idx+5] = topLeft;
        }
    }
    return indices;
}

// Generate indices for outline
function createOutlineIndices(nDiv, mDiv) {
    let part1Size = nDiv * (mDiv - 1),
        part2Size = mDiv * (nDiv - 1);
    let outlineIndices = new Uint16Array((part1Size + part2Size) * INDICES_PER_LINE);

    for (let nIndex = 0; nIndex < nDiv; nIndex += 1) {
        for (let mIndex = 0; mIndex < mDiv - 1; mIndex += 1) {
            let idx = (nIndex * (mDiv - 1) + mIndex) * INDICES_PER_LINE;
            let topIndex = mIndex * nDiv + nIndex,
                bottomIndex = topIndex + nDiv;
            outlineIndices[idx  ] = topIndex;
            outlineIndices[idx+1] = bottomIndex;
        }
    }

    for (let mIndex = 0; mIndex < mDiv; mIndex += 1) {
        for (let nIndex = 0; nIndex < nDiv - 1; nIndex += 1) {
            let idx = (part1Size + mIndex * (nDiv - 1) + nIndex) * INDICES_PER_LINE;
            let leftIndex = mIndex * nDiv + nIndex,
                rightIndex = leftIndex + 1;
            outlineIndices[idx  ] = leftIndex;
            outlineIndices[idx+1] = rightIndex;
        }
    }

    return outlineIndices;
}

function getCoord(outCoord, data, index) {
    outCoord[X_INDEX] = data[index+X_INDEX];
    outCoord[Y_INDEX] = data[index+Y_INDEX];
    outCoord[Z_INDEX] = data[index+Z_INDEX];
    outCoord[W_INDEX] = data[index+W_INDEX];

    return outCoord;
}

class GeometrySide {

    static gl;

    constructor(gl, n, m, coordFunc) {
        if (!GeometrySide.gl) { GeometrySide.gl = gl; }

        this.n = n;
        this.m = m;

        this.data = new Float32Array(this.n * this.m * COORD_SIZE);
        this.newCoordData = new Float32Array(this.n * this.m * COORD_SIZE);

        let coord = new Float32Array(COORD_SIZE);
        for (let mIndex = 0; mIndex < this.m; mIndex += 1) {
            for (let nIndex = 0; nIndex < this.n; nIndex += 1) {
                coordFunc(coord, nIndex, mIndex);
                let idx = (mIndex * this.n + nIndex) * COORD_SIZE;
                this.data[idx+X_INDEX] = coord[X_INDEX];
                this.data[idx+Y_INDEX] = coord[Y_INDEX];
                this.data[idx+Z_INDEX] = coord[Z_INDEX];
                this.data[idx+W_INDEX] = 0.0;
            }
        }

        this.vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, this.data, gl.DYNAMIC_DRAW);
        
        let cubeIndicesTop = createQuadIndices(this.n, this.m);
        this.indVboLength = cubeIndicesTop.length;

        this.indVbo = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indVbo);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, cubeIndicesTop, gl.STATIC_DRAW);

        let cubeOutlineIndicesTop = createOutlineIndices(this.n, this.m);
        this.indOutlineVboLength = cubeOutlineIndicesTop.length;

        this.indOutlineVbo = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indOutlineVbo);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, cubeOutlineIndicesTop, gl.STATIC_DRAW);
    }

    update(waveModel, timeFactor, geometryParams) {
        let gl = GeometrySide.gl;

        let coord = new Float32Array(COORD_SIZE),
            outputCoord = new Float32Array(COORD_SIZE),
            displacement = new Float32Array(COORD_SIZE);

        for (let mIndex = 0; mIndex < this.m; mIndex += 1) {
            for (let nIndex = 0; nIndex < this.n; nIndex += 1) {
                let idx = (mIndex * this.n + nIndex) * COORD_SIZE;
                getCoord(coord, this.data, idx);
                waveModel.getDisplacement(displacement, coord, timeFactor, geometryParams);
                coordAdd(outputCoord, coord, displacement);

                this.newCoordData[idx+X_INDEX] = outputCoord[X_INDEX];
                this.newCoordData[idx+Y_INDEX] = outputCoord[Y_INDEX];
                this.newCoordData[idx+Z_INDEX] = outputCoord[Z_INDEX];
                this.newCoordData[idx+W_INDEX] = outputCoord[W_INDEX];
            }
        }
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, this.newCoordData);
    }

    render(program) {
        let gl = GeometrySide.gl;

        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        
        // Draw surface
        gl.uniform3fv(program.uniformLocations['u_color'], QUAD_COLOR);

        gl.polygonOffset(1, 0);
        gl.enable(gl.POLYGON_OFFSET_FILL);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indVbo);
        gl.vertexAttribPointer(A_VERTEX_POSITION, 3, gl.FLOAT, false, COORD_SIZE * SIZE_OF_FLOAT, 0);
        gl.drawElements(gl.TRIANGLES, this.indVboLength, gl.UNSIGNED_SHORT, 0);
        
        // Draw Coordinate Lines
        gl.uniform3fv(program.uniformLocations['u_color'], OUTLINE_COLOR);

        gl.polygonOffset(0, 0);
        gl.disable(gl.POLYGON_OFFSET_FILL);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indOutlineVbo);
        gl.vertexAttribPointer(A_VERTEX_POSITION, 3, gl.FLOAT, false, COORD_SIZE * SIZE_OF_FLOAT, 0);
        gl.drawElements(gl.LINES, this.indOutlineVboLength, gl.UNSIGNED_SHORT, 0);
    }
}

class Geometry {

    static gl;

    constructor(gl, name, program, geometrySize, geometryDiv) {
        if (!Geometry.gl) { Geometry.gl = gl; }

        this.name = name;
        this.quadProgram = program;

        let geometryOrigin = [-geometrySize[X_INDEX] / 2, -geometrySize[Y_INDEX] / 2, -geometrySize[Z_INDEX] / 2];

        this.geometryParams = {
            waveDirLength: geometrySize[Z_INDEX],
            waveOrigin: geometryOrigin[Z_INDEX],
            yDepth: geometrySize[Y_INDEX],
            yTopLevel: geometryOrigin[Y_INDEX] + geometrySize[Y_INDEX],
        };

        // Calculate initial coordinates
        let dx = geometrySize[X_INDEX] / (geometryDiv[X_INDEX] - 1),
            dy = geometrySize[Y_INDEX] / (geometryDiv[Y_INDEX] - 1),
            dz = geometrySize[Z_INDEX] / (geometryDiv[Z_INDEX] - 1);

        // Top
        this.topSide = new GeometrySide(gl, geometryDiv[X_INDEX], geometryDiv[Z_INDEX], function(coord, n, m) {
            coord[X_INDEX] = geometryOrigin[X_INDEX] + n * dx;
            coord[Y_INDEX] = geometryOrigin[Y_INDEX] + geometrySize[Y_INDEX];
            coord[Z_INDEX] = geometryOrigin[Z_INDEX] + m * dz;
        });

        // Left
        this.leftSide = new GeometrySide(gl, geometryDiv[Y_INDEX], geometryDiv[Z_INDEX], function(coord, n, m) {
            coord[X_INDEX] = geometryOrigin[X_INDEX];
            coord[Y_INDEX] = geometryOrigin[Y_INDEX] + n * dy;
            coord[Z_INDEX] = geometryOrigin[Z_INDEX] + m * dz;
        });

        // Front
        this.frontSide = new GeometrySide(gl, geometryDiv[X_INDEX], geometryDiv[Y_INDEX], function(coord, n, m) {
            coord[X_INDEX] = geometryOrigin[X_INDEX] + n * dx;
            coord[Y_INDEX] = geometryOrigin[Y_INDEX] + m * dy;
            coord[Z_INDEX] = geometryOrigin[Z_INDEX] + geometrySize[Z_INDEX];
        });

        this.geometryOrigin = geometryOrigin;
    }

    update(waveModel, timeFactor) {
        this.topSide.update(waveModel, timeFactor, this.geometryParams);
        this.leftSide.update(waveModel, timeFactor, this.geometryParams);
        this.frontSide.update(waveModel, timeFactor, this.geometryParams);
    }

    render(projectionMatrix, viewMatrix) {
        let gl = Geometry.gl;

        gl.useProgram(this.quadProgram.program);

        gl.uniformMatrix4fv(this.quadProgram.uniformLocations['u_projection'], false, projectionMatrix);
        gl.uniformMatrix4fv(this.quadProgram.uniformLocations['u_view'], false, viewMatrix);

        this.topSide.render(this.quadProgram);
        this.leftSide.render(this.quadProgram);
        this.frontSide.render(this.quadProgram);
    }
}
