'use strict'

class BaseModel {
    constructor() {
        this.waveAmplitude = INITIAL_AMPLITUDE;
        this.wavePeriod = 1.0 / INITIAL_PERIOD;
        this.waveDissipation = INITIAL_DISSIPATION;
    }

    setPeriod(newPeriod) {
        this.wavePeriod = 1.0 / newPeriod;
    };

    setDissipation(newDissipation) {
        this.waveDissipation = newDissipation;
    };

    getDisplacement(outDisplacement, coord, timeFactor, geometryParams) {
        outDisplacement[X_INDEX] = 0.0;
        outDisplacement[Y_INDEX] = 0.0;
        outDisplacement[Z_INDEX] = 0.0;
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}

class RayleighWaveModel extends BaseModel {
    constructor() {
        super();

        // Mechanical parameters
        let E = 1.0; // Imaginary material
        this.rho = 10.0; // Imaginary material
        let nu = 0.3; // Let's stick to classics

        // Lame' parameters
        this.lambda = nu * E / ((1 + nu) * (1 - 2 * nu));
        this.mu = E / (2 * (1 + nu));

        // Approx. solution of Rayleigh wave
        this.thetaR = (0.87 + 1.12 * nu) / (1 + nu);

        this.timeFactorScale = 0.5; // To slow down Rayleigh waves a little bit
    }

    getDisplacement(outDisplacement, coord0, timeFactor, geometryParams) {
        let scale = this.wavePeriod / 5.0;
        let omega = 2.5;
        let A = this.waveAmplitude * scale;

        const zScale = 0.25;
        let coord = [
            coord0[X_INDEX] * scale,
            coord0[Y_INDEX] * scale,
            coord0[Z_INDEX] * scale * zScale,
            coord0[W_INDEX] * scale
        ];

        // Depth of a point
        const dissipationScale = 2.0;
        let depth = geometryParams.yTopLevel * scale - coord[Y_INDEX];
        let delta = depth / geometryParams.yDepth * this.waveDissipation * dissipationScale;

        // Rayleigh wave numbers for longitudinal and transversal waves
        let cL = omega * Math.sqrt(this.rho / (this.lambda  + 2 * this.mu));
        let cT = omega * Math.sqrt(this.rho / this.mu);

        // Rayleigh wave velocity
        let c = cT / Math.sqrt(this.thetaR);

        let qR = Math.sqrt(c * c - cL * cL);
        let sR = Math.sqrt(c * c - cT * cT);

        let amplitude = [
            0.0,
            A * c,
            A * qR
        ];

        let dissipation = [
            0.0,
            Math.exp(-qR * delta) - 2 * qR * sR / (cT * cT) * Math.exp(-sR * delta),
            Math.exp(-qR * delta) - 2 * c * c / (2 * c * c - cT * cT) * Math.exp(-sR * delta)
        ];

        let phi = c * coord[Z_INDEX] - omega * timeFactor * this.timeFactorScale;

        outDisplacement[X_INDEX] = 0.0;
        outDisplacement[Y_INDEX] = amplitude[Y_INDEX] * dissipation[Y_INDEX] * Math.cos(phi - Math.PI / 2.0);
        outDisplacement[Z_INDEX] = amplitude[Z_INDEX] * dissipation[Z_INDEX] * Math.cos(phi);
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}

class LoveWaveModel extends BaseModel {
    constructor() {
        super();
    }

    getDisplacement(outDisplacement, coord, timeFactor, geometryParams) {
        let distance = coord[Z_INDEX] - geometryParams.waveOrigin;
        let depth = geometryParams.yTopLevel - coord[Y_INDEX];
        let delta = depth / geometryParams.yDepth;

        outDisplacement[X_INDEX] = this.waveAmplitude * Math.exp(-this.waveDissipation * delta) *
            Math.cos(this.wavePeriod * distance - timeFactor);
        outDisplacement[Y_INDEX] = 0.0;
        outDisplacement[Z_INDEX] = 0.0;
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}

class PressureWaveModel extends BaseModel {
    constructor() {
        super();
        this.dissipationFactor = 1.0 / 4.0;
    }

    getDisplacement(outDisplacement, coord, timeFactor, geometryParams) {
        let distance = coord[Z_INDEX] - geometryParams.waveOrigin;
        let delta = distance / geometryParams.waveDirLength;

        outDisplacement[X_INDEX] = 0.0;
        outDisplacement[Y_INDEX] = 0.0;
        outDisplacement[Z_INDEX] = this.waveAmplitude *
            Math.exp(-this.waveDissipation * delta * this.dissipationFactor) *
            Math.cos(this.wavePeriod * distance - timeFactor);
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}

class ShearWaveModel extends BaseModel {
    constructor() {
        super();
        this.dissipationFactor = 1.0 / 4.0;
    }

    getDisplacement(outDisplacement, coord, timeFactor, geometryParams) {
        let distance = coord[Z_INDEX] - geometryParams.waveOrigin;
        let delta = distance / geometryParams.waveDirLength;

        outDisplacement[X_INDEX] = 0.0;
        outDisplacement[Y_INDEX] = this.waveAmplitude *
            Math.exp(-this.waveDissipation * delta * this.dissipationFactor) *
            Math.cos(this.wavePeriod * distance - timeFactor);
        outDisplacement[Z_INDEX] = 0.0;
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}

class AsymLambWaveModel extends BaseModel {
    constructor() {
        super();
    }

    getDisplacement(outDisplacement, coord, timeFactor, geometryParams) {
        let distance = coord[Z_INDEX] - geometryParams.waveOrigin;
        let depth = geometryParams.yTopLevel - coord[Y_INDEX];

        let phi = this.wavePeriod * distance - timeFactor;
        let theta = Math.sin(phi);

        let lambDistance = depth * 0.1;

        outDisplacement[X_INDEX] = 0.0;
        outDisplacement[Y_INDEX] = lambDistance * Math.cos(theta) + this.waveAmplitude * Math.cos(phi);
        outDisplacement[Z_INDEX] = lambDistance * Math.sin(theta);
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}

class SymLambWaveModel extends BaseModel {
    constructor() {
        super();
    }

    getDisplacement(outDisplacement, coord, timeFactor, geometryParams) {
        let distance = coord[Z_INDEX] - geometryParams.waveOrigin;

        let phi = this.wavePeriod * distance - timeFactor;

        let depth = coord[Y_INDEX];
        let lambDistance = depth / geometryParams.yDepth;

        outDisplacement[X_INDEX] = 0.0;
        outDisplacement[Y_INDEX] = this.waveAmplitude * lambDistance * Math.cos(phi);
        outDisplacement[Z_INDEX] = this.waveAmplitude * Math.cos(lambDistance) * Math.sin(phi);
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}

class RadialPressureWaveModel extends BaseModel {
    constructor() {
        super();
        this.radialCoord = new Float32Array(4);
        this.amplitudeScale = 0.35;
    }

    getDisplacement(outDisplacement, coord, timeFactor, geometryParams) {
        let radialWaveOrigin = new Float32Array([0.0, geometryParams.yTopLevel, 0.0, 0.0]);
        convertCartesianToRadial(this.radialCoord, coord, radialWaveOrigin);

        let q = this.waveAmplitude * this.amplitudeScale * Math.cos(this.wavePeriod * this.radialCoord[R_INDEX] - timeFactor);

        outDisplacement[X_INDEX] = q * Math.cos(this.radialCoord[THETA_INDEX]);
        outDisplacement[Y_INDEX] = 0.0;
        outDisplacement[Z_INDEX] = q * Math.sin(this.radialCoord[THETA_INDEX]);
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}

class RadialShearWaveModel extends BaseModel {
    constructor() {
        super();
        this.radialCoord = new Float32Array(4);
        this.amplitudeScale = 0.35;
    }

    getDisplacement(outDisplacement, coord, timeFactor, geometryParams) {
        let radialWaveOrigin = new Float32Array([0.0, geometryParams.yTopLevel, 0.0, 0.0]);
        convertCartesianToRadial(this.radialCoord, coord, radialWaveOrigin);

        let q = this.waveAmplitude * this.amplitudeScale * Math.cos(this.wavePeriod * this.radialCoord[R_INDEX] - timeFactor);

        outDisplacement[X_INDEX] = 0.0;
        outDisplacement[Y_INDEX] = q * Math.sin(this.radialCoord[R_INDEX]);
        outDisplacement[Z_INDEX] = 0.0;
        outDisplacement[W_INDEX] = 0.0;

        return outDisplacement;
    }
}
